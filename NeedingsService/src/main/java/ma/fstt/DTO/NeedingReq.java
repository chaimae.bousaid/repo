package ma.fstt.DTO;

import lombok.Builder;
import lombok.Data;
import ma.fstt.Persistence.Needing;

@Data
@Builder
public class NeedingReq {
    private String name;
    private String description;

    private String location;

    private String status;

    private Needing.Category category;

    private long userId;
}
