package ma.fstt.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.fstt.Persistence.Needing;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NeedingResp {
    private String name;
    private String description;

    private String location;

    private String status;

    private Needing.Category category;

    private long userId;
}


